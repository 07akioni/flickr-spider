import fs.api as api
import fs.script as script
import time
import json

if __name__ == '__main__':
    script.downloadNewYorkData()
    script.downloadDetailedUserInfo()
    script.downloadDetailedPhotoInfo()
        