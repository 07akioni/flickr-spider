import requests
import bs4
# 还需要 pip install html5lib

proxies = {
  "http": "http://127.0.0.1:1087",
  "https": "http://127.0.0.1:1087",
}

def getFlickrPersonProfile(personId):
    url = 'https://api.flickr.com/services/rest?datecreate=1&extras=icon_urls%2Csafe_search%2Cgalleries_view_layout_pref%2C%20has_stats%2Cexpand_bbml%2Csocial_urls%2Cwith_stats&'\
        + ('user_id=%s' % personId.replace('@', '%40'))\
        + '&viewerNSID=&method=flickr.profile.getProfile&csrf=&api_key=cccd468e28b66d260c51c7d340af0f65&format=json&hermes=1&hermesClient=1&reqId=e9968290&nojsoncallback=1'
    response = requests.get(url, proxies=proxies)
    print(response.text)

def getFlickrPersonContacts(personId, page):
    url = 'https://www.flickr.com/people/%s/contacts/?filter=&page=%d' % ('124037962@N06', 1)
    response = requests.get(url, proxies=proxies)
    printAllNameInContactPage(response.text)

def printAllNameInContactPage(html):
    soup = bs4.BeautifulSoup(html, 'html5lib')
    trs = soup.select('tr')
    for tr in trs:
        for h2 in tr.select('h2'):
            print(h2.text)

# getFlickrPersonProfile('124037962@N06')
getFlickrPersonContacts('124037962@N06', 1)