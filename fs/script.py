import fs.api as api
import os
import json
import time
import traceback
import re

def downloadDetailedPhotoInfo():
    s = set()
    count = 0
    files = os.listdir('data/')
    for fileName in files:
        if not re.compile('\d+-\d+-p\d+.json').match(fileName):
            continue
        infoListFile = open('data/' + fileName, 'r')
        infoList = json.loads(infoListFile.read())
        for photo in infoList['photo']:
            count += 1
            print(count)
            try:
                file_name = 'detail/photo/' + str(photo['id']) + '.json'
                if os.path.exists(file_name):
                    print('data exists:', file_name)
                    continue
                time.sleep(3)
                info = api.getPhotoInfo(photo['id'])
                photoInfoFile = open(file_name, 'w')
                photoInfoFile.write(json.dumps(info))
                photoInfoFile.close()
            except Exception:
                traceback.print_exc()
                print('error')
                pass

def downloadDetailedUserInfo():
    s = set()
    count = 0
    files = os.listdir('data/')
    for fileName in files:
        if not re.compile('\d+-\d+-p\d+.json').match(fileName):
            continue
        infoListFile = open('data/' + fileName, 'r')
        infoList = json.loads(infoListFile.read())
        for photo in infoList['photo']:
            count += 1
            print(count)
            try:
                file_name = 'detail/user/' + str(photo['owner']) + '.json'
                if os.path.exists(file_name):
                    print('data exists:', file_name)
                    continue
                time.sleep(3)
                info = api.getPersonProfile(photo['owner'])
                photoInfoFile = open(file_name, 'w')
                photoInfoFile.write(json.dumps(info))
                photoInfoFile.close()
            except Exception:
                traceback.print_exc()
                print('error')
                pass

def downloadNewYorkData():
    """
    comment:
        New York Parameters: '-74.1, 40.3, -73.5, 40.6',16,1,'2018-12-1','2019-2-13',2,500
        Parameters after correction: 'start_lat': -74.25, 'end_lat': -73.68, 'start_lon': 40.5, 'end_lon': 41.0,
    warning:
        注意如果出现网络错误只会单纯的跳过，所以可能出现下不全的情况= =
    note:
        first: -74.1, 40.3, -73.5, 40.6
        second: -74.7, 40.3, -74.1, 40.6
    """
    page = 1
    for year in range(2004, 2020):
        for month in range(1, 13):
            print(year, month)
            try:
                page = 0
                while True:
                    file_name = 'data/%d-%d-p%d.json' % (year, month, page + 1)
                    if os.path.exists(file_name):
                        print('data exists:', file_name)
                        page += 1
                        continue
                    page += 1
                    time.sleep(3)
                    if month == 12:
                        photos = api.photosSearchByLocation('-74.7, 40.3, -74.1, 40.6',11,1,'%d-%d-1' % (year, month),'%d-%d-31' % (year, month),page,500,'date-posted-desc')['photos']
                    else:
                        photos = api.photosSearchByLocation('-74.7, 40.3, -74.1, 40.6',11,1,'%d-%d-1' % (year, month),'%d-%d-1' % (year, month + 1),page,500,'date-posted-desc')['photos']
                    if len(photos['photo']) > 0:
                        photosFile = open(file_name, 'w')
                        photosFile.write(json.dumps(photos))
                        photosFile.close()
                    current_page = photos['page']
                    total_page = photos['pages']
                    print('monthly data size:', photos['total'])
                    print('pages:', current_page, '/', total_page)
                    if current_page >= total_page:
                        break
            except Exception as e:
                traceback.print_exc()
                continue
            finally:
                pass