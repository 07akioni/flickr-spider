# zgd flickr api

import json
import flickrapi
import pickle
import pprint

class ApiError(Exception):
    pass

class ResponseParseError(Exception):
    pass

class NetworkError(Exception):
    pass

api_key = '1583b37dc3bc439ed0c2f782676d22e1'
api_secret = '2d3f46ae63a9abff'

flickr = flickrapi.FlickrAPI(api_key,api_secret,format='json')

def getPersonProfile(user_id):
    profile = flickr.profile.getProfile(user_id=user_id)
    print(json.loads(profile.decode()))
    return json.loads(profile.decode())

def getPublicList(user_id,page,per_page):
    contacts = flickr.contacts.getPublicList(user_id=user_id,page=page,per_page=per_page)
    print(repr(contacts))

#photo还可以通过text与tag选择
def photosSearchByUser(user_id,min_taken_date,max_taken_date,page,per_page):
    photos = flickr.photos.search(user_id=user_id,min_taken_date=min_taken_date,max_taken_date=max_taken_date,page=page,per_page=per_page)
    print(repr(photos))

#bbox是选定的的区域（经纬度），accuracy是精度要求（16为精确到街道）,content_type是数据类型（1表示仅获得图片）
def photosSearchByLocation(bbox, accuracy, content_type, min_taken_date, max_taken_date, page, per_page, sort):
    try:
        photos = flickr.photos.search(bbox=bbox,min_taken_date=min_taken_date,max_taken_date=max_taken_date,page=page,sort=sort)
    except Exception:
        raise NetworkError()
    try:
        photos = json.loads(bytes.decode(photos))
        if photos['stat'] != 'ok':
            raise ApiError()
        return photos
        # pprint.pprint(photos['photos']['pages'])
        # for photo in photos['photos']['photo']:
        #     print(photo)
        #     pass
    except Exception as e:
        if isinstance(e, ApiError):
            print(photos)
            raise ApiError()
        else:
            raise ResponseParseError()
    # print(photos)

#获得photo的基本信息（id，title，tag，taken_time，coordinates，location，url）
def getPhotoInfo(photoId):
    tag = []
    text = flickr.photos.getInfo(photo_id=photoId,format='json')
    parsed_data = json.loads(text.decode())
    # pprint.pprint(parsed_data)
    # text_tag = parsed_data['photo']['tags']['tag']
    # for line in text_tag:
    #     tag.append(line['_content'])
    # longitude = parsed_data['photo']['location']['longitude']
    # latitude = parsed_data['photo']['location']['latitude']
    # time = parsed_data['photo']['dates']['taken']
    # url_loc = parsed_data['photo']['urls']['url']
    # url = url_loc[0]['_content']
    # location = parsed_data['photo']['location']['locality']['_content']
    # location = []
    # title = parsed_data['photo']['title']['_content']
    # photo_id = parsed_data['photo']['id']
    # info = {
    #     "photo_id":photo_id,
    #     "title":title,
    #     "tag":tag,
    #     "taken_time":time,
    #     "coordinates": (latitude, longitude),
    #     'location':location,
    #     'url':url
    # }
    return parsed_data

if __name__ == '__main__':
    #photosSearchByUser('124037962@N06','2016-1-1','2018-1-1',1,1000)
    #photosSearchByLocation('-74.1, 40.3, -73.5, 40.6',16,1,'2019-1-1','2019-2-13',2,500)
    print(getPhotoInfo('32723541508'))